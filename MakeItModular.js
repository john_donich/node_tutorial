/**
 * Created by johnmdonich on 2/18/15.
 */

var myArgv = process.argv;
var listFilesByType = require('./CountFilesByType');

listFilesByType(myArgv[2], myArgv[3], function (err, filteredFiles) {
    if (err != null) {
        console.log("Sorry an error occurred during your request", err);
    } else {
        filteredFiles.forEach(function (filteredFile) {
            console.log(filteredFile);
        })
    }
});

