/**
 * Created by johnmdonich on 2/19/15.
 */
var bl = require('bl');
var http = require('http');
var urls = process.argv.slice(2);
var responses = [];
var waiting = 0;

var printResponse = function () {
    if (!waiting) {
        responses.map(function(response) {console.log(response)});
    }
};

function getUrlsInOrder (urlId) {
    waiting++;
    http.get(urls[urlId], function getDataStreamCallback (resoponse) {
        resoponse.pipe(bl(function (error, data) {
            if (error) {
                console.error(error);
            }
            else {
                responses[urlId] = data.toString();
                waiting--;
                printResponse();
            }
        }));
        resoponse.on('error', console.error);

    });
}

for(var urlId in urls) {
    getUrlsInOrder(urlId);
}

/*

** Their Solution **


 var http = require('http')
 var bl = require('bl')
 var results = []
 var count = 0

 function printResults () {
    for (var i = 0; i < 3; i++)
        console.log(results[i])
}

 function httpGet (index) {
    http.get(process.argv[2 + index], function (response) {
        response.pipe(bl(function (err, data) {
            if (err)
                return console.error(err)

            results[index] = data.toString()
            count++

            if (count == 3)
                printResults()
        }))
    })
}

 for (var i = 0; i < 3; i++)
 httpGet(i)
*/
