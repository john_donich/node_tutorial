/**
 * Created by johnmdonich on 2/19/15.
 */

var bl = require('bl');
var http = require('http');
var myArgv = process.argv;

function getDataStreamCallback (resoponse) {
    resoponse.pipe(bl(function (error, data) {
        if (error) {
            console.error(error);
        }
        else {
            console.log(data.toString().length);
            console.log(data.toString());
        }
    }));
    resoponse.on('error', console.error);
}

http.get(myArgv[2], getDataStreamCallback);

