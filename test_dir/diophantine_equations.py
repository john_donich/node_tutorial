# Program to determine values of a, b, and c for the
# diophantine equation 6a+9b+20c = n (total number of chicken nuggets possible)
# and the values of n after which consecutive integers can be purchased

s
a_coeff=0
b_coeff=0
c_coeff=0
a = 0
b = 0
c = 0
n = 1
stop = 0
n_set=[]
test_n = 0

a_coeff= int(raw_input("Enter 1st integer coeffiecient value: "))
b_coeff= int(raw_input("Enter 2st integer coeffiecient value: "))
c_coeff= int(raw_input("Enter 3st integer coeffiecient value: "))



while stop!=-1:                                 # Loop with condition that is false until 5 consecutive values of n have been solved for
    while ((c_coeff*c)<=n):                          # Looping though the values for c 
        while ((b_coeff*b)<=n):                       # Looping through the vaues for b
            while ((a_coeff*a)<=n):                   # Looping through the vaues for a
                test_n=(a_coeff*a)+(b_coeff*b)+(c_coeff*c)
                if test_n==n:                   # The test determining if the equation is equal to n
                    print 'n='+ str(n) +', a='+ str(a) +', b=' + str(b) + ', c='+ str(c)
                    n_set.append(n)
                    if len(n_set)>1:            # removing repeat values of n in the set of true n's 
                        if n_set[-1]==n_set[-2]:
                            n_set.remove(n_set[-1])
                    if len(n_set)>5:                    # Testing to see if 5 consecutive n's have occured
                        if ((n_set[-1]-n_set[-6])==5):  # Condition that tests for 5 consecutive values of n
                            stop = -1                          
                a+=1

            b+=1
            a=0
        c+=1
        a=0
        b=0
    c=0   
    n+=1
print n_set
print "the number n after which consecutive n values can be purchased is " + str(n_set[-6]-1)

##for p in range(1,len(n_set)):                        # Creation of a set with the values of n for which equation is false
##    if n_set.count(p)==0:
##        n_false.append(p)
##
##print n_false





