/**
 * Created by johnmdonich on 2/22/15.
 */

var http = require('http');
var map = require('through2-map');
var url = require('url');
var server = http.createServer(function (request, response) {
    response.writeHead(200, { 'content-type': 'application/json' });
    if (request.method == 'GET') {
        var urlObject = url.parse(request.url, true);
        var query = urlObject.query;
        var path = urlObject.path;
        if (path.indexOf('parsetime') > -1) {
            var isoDate = new Date(query['iso']);
            var timeResponse = {hour: isoDate.getHours(), minute: isoDate.getMinutes(), second: isoDate.getSeconds()};
            response.write(JSON.stringify(timeResponse));
            request.pipe(response);
        }
        if (path.indexOf('unixtime') > -1) {
            var isoDate = new Date(query['iso']);
            var unixTimeResponse = {unixtime: isoDate.getTime()};
            response.write((JSON.stringify(unixTimeResponse)));
            request.pipe(response);
        }
    }
    else {
        console.log(request.method);
        console.error("Not A GET");
    }
});
server.listen(process.argv[2]);


/** Their Solution
 *
 * var http = require('http')
 var url = require('url')

 function parsetime (time) {
      return {
        hour: time.getHours(),
        minute: time.getMinutes(),
        second: time.getSeconds()
      }
    }

 function unixtime (time) {
      return { unixtime : time.getTime() }
    }

 var server = http.createServer(function (req, res) {
      var parsedUrl = url.parse(req.url, true)
      var time = new Date(parsedUrl.query.iso)
      var result

      if (/^\/api\/parsetime/.test(req.url))
        result = parsetime(time)
      else if (/^\/api\/unixtime/.test(req.url))
        result = unixtime(time)

      if (result) {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(result))
      } else {
        res.writeHead(404)
        res.end()
      }
    })
 server.listen(Number(process.argv[2]))

 *
 */
