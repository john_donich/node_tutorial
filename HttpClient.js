/**
 * Created by johnmdonich on 2/19/15.
 */

var myArgv = process.argv;
var http = require('http');
function getDataEmitterCallback (resoponse) {
    resoponse.setEncoding('utf8');
    resoponse.on('data', console.log);
    resoponse.on('error', console.error)
}

http.get(myArgv[2], getDataEmitterCallback);